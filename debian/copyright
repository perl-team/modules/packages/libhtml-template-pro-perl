Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: HTML-Template-Pro
Upstream-Contact: Igor Yu. Vlasenko <viy@altlinux.org>
Source: https://metacpan.org/release/HTML-Template-Pro

Files: *
Copyright:
 2005-2021, Igor Yu. Vlasenko <viy@altlinux.org>
 2000-2009, Sam Tregar <sam@tregar.com>
License: Artistic or GPL-2+

Files: expr.c
Copyright: 1984-2018, Free Software Foundation, Inc.
License: GPL-3+ with Bison exception

Files: ppport.h
Copyright:
 2001, Paul Marquess <pmqs@cpan.org> (Version 2.x)
 1999, Kenneth Albanowski <kjahds@kjahds.com> (Version 1.x)
License: Artistic or GPL-1+

Files: debian/*
Copyright:
 2007-2008, Damyan Ivanov <dmn@debian.org>
 2007, Vincent Danjean <vdanjean@debian.org>
 2008-2022, gregor herrmann <gregoa@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2009, Franck Joncourt <franck.mail@dthconnex.com>
 2009, Jonathan Yu <jawnsy@cpan.org>
 2009, Jose Luis Rivas <ghostbar38@gmail.com>
 2010-2012, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+ with Bison exception
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.
 .
 As a special exception, you may create a larger work that contains
 part or all of the Bison parser skeleton and distribute that work
 under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof
 as a parser skeleton.  Alternatively, if you modify or redistribute
 the parser skeleton itself, you may (at your option) remove this
 special exception, which will cause the skeleton and the resulting
 Bison output files to be licensed under the GNU General Public
 License without this special exception.
 .
 This special exception was added by the Free Software Foundation in
 version 2.2 of Bison.
