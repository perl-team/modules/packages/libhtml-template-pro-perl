libhtml-template-pro-perl (0.9524-1) unstable; urgency=medium

  * Import upstream version 0.9524.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 20 Jan 2022 21:16:59 +0100

libhtml-template-pro-perl (0.9523-1) unstable; urgency=medium

  * Import upstream version 0.9523.

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Dec 2021 16:26:27 +0100

libhtml-template-pro-perl (0.9522-1) unstable; urgency=medium

  * Import upstream version 0.9522.
  * Fix typo in previous changelog entry.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Dec 2021 00:44:29 +0100

libhtml-template-pro-perl (0.9521-1) unstable; urgency=medium

  * Import upstream version 0.9521.
    - added pcre2 support (cf. #1000095)
  * Add build dependency on libpcre2-dev, and don't disable PCRE support in
    debian/rules any longer.
  * Update years of upstream and third-party copyright.
  * Refresh optint.patch (offset).

 -- gregor herrmann <gregoa@debian.org>  Thu, 02 Dec 2021 17:45:04 +0100

libhtml-template-pro-perl (0.9510-3) unstable; urgency=medium

  * Build without PCRE support.
    Remove libpcre3-dev from Build-Depends, and pass PCRE=0 to
    dh_auto_configure. (Closes: #1000095)
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Nov 2021 00:39:45 +0100

libhtml-template-pro-perl (0.9510-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team. Closes: #924987
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Jose Luis Rivas from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.

  * debian/control: update Build-Depends for cross builds.
    and
  * Annotate test-only build dependencies with <!nocheck>.
    Closes: #979080

  * Bump debhelper-compat to 13.
  * Update years of packaging copyright.
  * Enable all hardening flags.
  * debian/rules: remove override_dh_auto_clean which cleaned less than
    the default.
  * debian/copyright: update license short name.
  * Make autopkgtests functional.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Jan 2021 17:08:54 +0100

libhtml-template-pro-perl (0.9510-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
    Fixes "FTBFS with perl 5.18: POD syntax"
    (Closes: #708078)
  * Set Standards-Version to 3.9.4 (no changes).
  * Use debhelper 9.20120312 to get all hardening flags.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 May 2013 21:24:19 +0200

libhtml-template-pro-perl (0.9509-1) unstable; urgency=low

  * New upstream release
  * Raised standards version to 3.9.3
  * Updated copyright
  * Added a comparison with HTML::Template in the long description
  * Added a patch to stop build failure over optint.c and
    added overrode clean rules

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sun, 04 Mar 2012 13:56:16 +0000

libhtml-template-pro-perl (0.9508-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 28 Dec 2011 11:41:50 +0100

libhtml-template-pro-perl (0.9507-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Upload with medium urgency as this fixes a XSS vulnerability.
    (Closes: #652587)

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 18 Dec 2011 23:04:24 +0100

libhtml-template-pro-perl (0.9506-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright.
  * Bump debhelper compatibility level to 8.

 -- gregor herrmann <gregoa@debian.org>  Wed, 05 Oct 2011 18:53:32 +0200

libhtml-template-pro-perl (0.9505-1) unstable; urgency=low

  * Removed patch as it has been applied upstream
  * New upstream release
  * Raised standards version to 3.9.2
  * Updated copyright

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Mon, 04 Jul 2011 19:08:18 +0100

libhtml-template-pro-perl (0.9503-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * New upstream release
  * Upped standard to 3.9.1
  * Added myself to Uploaders
  * Reviewed spelling patch
  * Refreshed control
  * Reviewed and updated copyright

  [ gregor herrmann ]
  * debian/copyright: refresh License stanzas.
  * Remove version from libjson-perl (build) dependency, already satisfied on
    stable.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sun, 05 Sep 2010 22:39:53 +0100

libhtml-template-pro-perl (0.9502-1) unstable; urgency=low

  * New upstream release.
  * Set Standards-Version to 3.9.0 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Mon, 28 Jun 2010 18:04:28 +0200

libhtml-template-pro-perl (0.9501-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Jun 2010 01:56:52 +0200

libhtml-template-pro-perl (0.95-1) unstable; urgency=low

  * New upstream release.
  * Refresh DEP3 patch header.

 -- gregor herrmann <gregoa@debian.org>  Sun, 23 May 2010 17:04:07 +0200

libhtml-template-pro-perl (0.94-1) unstable; urgency=low

  * New upstream release.
  * Convert to source format 3.0 (quilt).
  * Set Standards-Version to 3.8.4 (no changes).
  * debian/copyright: update formatting and list of contributors to debian/*.
  * Add a patch to fix some spelling mistakes.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Mar 2010 19:36:44 +0200

libhtml-template-pro-perl (0.93-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 23 Nov 2009 19:15:55 -0500

libhtml-template-pro-perl (0.92-1) unstable; urgency=low

  * New upstream release
    + Appears to be minor bugfixes; no changelog entry was recorded
      upstream

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 29 Sep 2009 18:57:44 -0400

libhtml-template-pro-perl (0.90-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Add force_untaint option
    + Treat code references properly by evaluating them dynamically
    + Fix to support magic arrays and hashes
    + Void context output writes to built-in stdout
    + Add support for template tags decorated as XML
  * Now requires JSON 2 or later.
  * Drop perl version dependency as permitted by SV 3.8.3

  [ Ryan Niebur ]
  * Update jawnsy's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 19 Sep 2009 18:01:12 -0400

libhtml-template-pro-perl (0.87-1) unstable; urgency=low

  * New upstream release
    + Fix memory leak in expr
    + By default, the built-in find_file implementation is used now

 -- Jonathan Yu <frequency@cpan.org>  Sat, 29 Aug 2009 19:19:18 -0400

libhtml-template-pro-perl (0.86-1) unstable; urgency=low

  * New upstream release
    + Bugfix for case_sensitive=0 root keys
    + Bugfixes for expr and find_file
    + New future NULL expr type
    + New builtins: atan2, int, abs, defined, length

 -- Jonathan Yu <frequency@cpan.org>  Mon, 24 Aug 2009 07:55:23 -0400

libhtml-template-pro-perl (0.85-1) unstable; urgency=low

  [ Franck Joncourt ]
  * New upstream release
  * Bumped Standards-Version up to 3.8.3 (no changes).
  * Add /me to Uploaders.

  [ Jonathan Yu ]
  * Converted to short dh rules format
  * Clean up control description
  * Added myself to Uploaders and Copyright
  * Updated copyright for the distribution and for ppport.h

 -- Franck Joncourt <franck.mail@dthconnex.com>  Tue, 18 Aug 2009 22:30:20 +0200

libhtml-template-pro-perl (0.76-1) unstable; urgency=low

  [ Jose Luis Rivas ]
  * New upstream release

  [ gregor herrmann ]
  * Install the new TODO file.

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Tue, 14 Jul 2009 12:25:11 -0430

libhtml-template-pro-perl (0.75-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Jose Luis Rivas ]
  * New upstream release
  * Updated Standards-Version to 3.8.2, no changes needed.
  * Added me as uploader.

  [ gregor herrmann ]
  * debian/copyright: update years of upstream copyright.

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Thu, 09 Jul 2009 15:31:43 -0430

libhtml-template-pro-perl (0.74-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 Apr 2009 20:33:35 +0200

libhtml-template-pro-perl (0.73-1) unstable; urgency=low

  * New upstream release.
  * Disable additional tests in debian/rules, remove build dependency on
    libipc-sharedcache-perl from debian/control.
  * Set Standards-Version to 3.8.1 (no changes).
  * debian/copyright: update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 09 Apr 2009 21:24:06 +0200

libhtml-template-pro-perl (0.72-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Add debian/README.ru to the newly created debian/clean and simplify
    debian/rules' clean target.
  * debian/copyright: add copyright and license information about third-party
    code.
  * Activate an additional test during build by setting TEST_SHARED_MEMORY in
    debian/rules and adding libipc-sharedcache-perl to Build-Depends in
    debian/control.
  * Use iconv instead of konwert in debian/rules for converting README.ru to
    UTF-8; saves us a build-dependency in debian/control.

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Dec 2008 23:45:10 +0100

libhtml-template-pro-perl (0.71-1) unstable; urgency=low

  * New upstream release.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * Change debian/copyright to the new format.

 -- gregor herrmann <gregoa@debian.org>  Mon, 08 Sep 2008 20:30:06 +0200

libhtml-template-pro-perl (0.70-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: extended regexp for matching upstream releases.
  * debian/copyright: update years of copyright.
  * Set Standards-Version to 3.8.0 (no changes).
  * Add /me to Uploaders.
  * Refresh debian/rules, no functional changes.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Jun 2008 22:27:42 +0200

libhtml-template-pro-perl (0.69-1) unstable; urgency=low

  * New upstream release.

 -- Roberto C. Sanchez <roberto@debian.org>  Sun, 16 Mar 2008 01:31:42 -0400

libhtml-template-pro-perl (0.68.1-1) unstable; urgency=low

  [ Jose Luis Rivas ]
  * New upstream release

  [ gregor herrmann ]
  * debian/rules: delete /usr/share/perl5 only if it exists.

  [ Damyan Ivanov ]
  * s/This module/HTML::Template::Pro/ in long description
  * debian/rules: stop setting VENDORARCH variables
  * debhelper compatibility level 6

 -- Damyan Ivanov <dmn@debian.org>  Mon, 21 Jan 2008 14:35:03 +0200

libhtml-template-pro-perl (0.67-1) unstable; urgency=low

  [ Vincent Danjean ]
  * add debian/watch file

  [ David Paleino ]
  * New upstream version
  * debian/control:
    - updating Standards-Version to 3.7.3
  * debian/watch - now using a dist-based URL
  * debian/rules - removing empty /usr/share/perl5/

  [ Damyan Ivanov ]
  * drop redundant README from docs, install FAQ instead
  * re-code README.ru in UTF-8
    + remove it in clean target
    + add konwert to Build-Depends
  * comment-out unused dh_installexamples
  * add "v?" to watchfile pattern
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Fri, 28 Dec 2007 15:52:02 +0200

libhtml-template-pro-perl (0.66-1) unstable; urgency=low

  * Initial Release. (Closes: #452034)
  * Packaged for Koha (http://www.koha.org/)

 -- Vincent Danjean <vdanjean@debian.org>  Tue, 20 Nov 2007 00:37:43 +0100
